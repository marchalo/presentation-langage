Exemple de création d'un langage de programmation
=================================================

Ce dépôt contient deux exemples:

- Un interpréteur du langage Additionneur.
- Un plugin IntelliJ pour le langage Additionneur.

License: MIT
