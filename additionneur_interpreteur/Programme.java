import generation.AdditionneurParser;
import interpreteur.AdditionneurInterpreteur;
import parser.PsiFichier;

import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.Reader;

/**
 * Programme principal de l'interpréteur de fichier du langage Additionneur.
 * @author Louis Marchand
 * @version 1.0, 8 avril 2024
 */
public class Programme {

    /**
     * Exécution du programme
     * @param aArguments Le paramêtre de ligne de commande permettant d'indiquer le fichier à interpréter.
     */
    public static void main(String[] aArguments) {
        if (aArguments.length < 1) {
            System.err.println("Vous devez spécifier un fichier source.\n");
        } else {
            try {
                Reader lReader = new FileReader(aArguments[0]);
                AdditionneurParser lParser = new AdditionneurParser(lReader);
                Object lPsiFichier = lParser.parse().value;
                if (lPsiFichier instanceof PsiFichier lFichier) {
                    AdditionneurInterpreteur lInterpreteur = new AdditionneurInterpreteur(lFichier);
                    lInterpreteur.execution();
                }
            } catch (FileNotFoundException lException) {
                System.err.println("Le fichier " + aArguments[0] + "n'est pas valide.\n");
            } catch (Exception lException) {
                System.err.println(lException.getMessage());
            }

        }
    }
}
