package interpreteur;

import parser.PsiAffichable;
import parser.PsiAffiche;
import parser.PsiAfficherFinLigne;
import parser.PsiAssignation;
import parser.PsiChaine;
import parser.PsiElement;
import parser.PsiExpression;
import parser.PsiFichier;
import parser.PsiImmediat;
import parser.PsiInstruction;
import parser.PsiLigne;
import parser.PsiLire;
import parser.PsiMoins;
import parser.PsiOperateurBinaire;
import parser.PsiPlus;

import java.util.Hashtable;
import java.util.Scanner;

/**
 * Un interpréteur pour le langage Additionneur.
 * @author Louis Marchand
 * @version 1.0, 8 avril 2024
 */
public class AdditionneurInterpreteur {

    /**
     * Les variables initialisé dans le `fichier` en cours
     */
    private Hashtable<String, Integer> variables;

    /**
     * Scanner utilisé pour lire de l'information de l'utilisateur.
     */
    private Scanner scanner;

    /**
     * Le fichier à interpréter.
     */
    private PsiFichier fichier;

    /**
     * Retourne le fichier à interpréter dans l'objet en cours.
     * @return le fichier à interpréter.
     */
    public PsiFichier getFichier() {
        return fichier;
    }

    /**
     * Initialise l'interpréteur
     * @param aFichier le `fichier` à interpréter.
     */
    public AdditionneurInterpreteur(PsiFichier aFichier) {
        fichier = aFichier;
        variables = new Hashtable<>();
        scanner = new Scanner(System.in);
    }

    /**
     * Exécute l'interprétation du `fichier`
     * @throws InterpreterException Lorsqu'une erreur d'interprétation est survenue.
     */
    public void execution() throws InterpreterException {
        PsiLigne lLigne = fichier.getPremiereLigne();
        while (lLigne != null) {
            executerInstruction(lLigne.getInstruction());
            lLigne = lLigne.getProchaineLigne();
        }
    }

    /**
     * Exécute l'interprétation d'une seule instruction
     * @param aInstruction l'instruction à interpréter
     * @throws InterpreterException Lorsqu'une erreur d'interprétation est survenue.
     */
    private void executerInstruction(PsiInstruction aInstruction) throws InterpreterException {
        if (aInstruction instanceof PsiAffiche aAffichage) {
            PsiAffichable lAffichable = aAffichage.getAffichable();
            System.out.print(valeurAffichable(lAffichable));
        } else if (aInstruction instanceof PsiAssignation lAssignation) {
            variables.put(lAssignation.getIdentificateur(),
                    valeurExpression(lAssignation.getExpression()));
        } else {
            genererErreur(aInstruction, "Instruction non valide.");
        }
    }

    /**
     * Calcul et retourne la valeur d'une expression.
     * @param aExpression l'expression à évaluer.
     * @return la valeur de l'expression après évaluation.
     * @throws InterpreterException Lorsqu'une erreur d'interprétation est survenue.
     */
    private Integer valeurExpression(PsiExpression aExpression) throws InterpreterException {
        Integer lResultat = 0;
        if (aExpression instanceof PsiLire) {
            String lLecture = scanner.nextLine();
            try {
                lResultat = Integer.parseInt(lLecture);
            } catch (NumberFormatException lException) {
                genererErreur(aExpression, "L'entrée " + lLecture +
                        " n'est pas un entier valide.");
            }
        } else if (aExpression instanceof PsiImmediat lImmediat) {
            lResultat = valeurImmediat(lImmediat);
        } else if (aExpression instanceof PsiOperateurBinaire lOperateur) {
            lResultat = valeurOperateurBinaire(lOperateur);
        } else {
            genererErreur(aExpression, "Expression non valide.");
        }
        return lResultat;
    }

    /**
     * Calcul et retourne la valeur après l'exécution d'un l'opétateur binaire.
     * @param aOperateur L'opérateur à interpréter
     * @return La valeur de l'opération après exécution.
     * @throws InterpreterException Lorsqu'une erreur d'interprétation est survenue.
     */
    private Integer valeurOperateurBinaire(PsiOperateurBinaire aOperateur) throws InterpreterException {
        Integer lResultat = 0;
        if (aOperateur instanceof PsiPlus) {
            lResultat = valeurImmediat(aOperateur.getImmediat()) +
                    valeurExpression(aOperateur.getExpression());
        } else if (aOperateur instanceof PsiMoins) {
            lResultat = valeurImmediat(aOperateur.getImmediat()) -
                    valeurExpression(aOperateur.getExpression());
        } else {
            genererErreur(aOperateur, "Opérteur non valide.");
        }
        return lResultat;
    }

    /**
     * Retourne la valeur d'un immédiat.
     * @param aImmediat L'immédiat à évaluer.
     * @return La valeur de l'immédiat
     * @throws InterpreterException Lorsqu'une erreur d'interprétation est survenue.
     */
    private Integer valeurImmediat(PsiImmediat aImmediat) throws InterpreterException {
        Integer lResultat = 0;
        if (aImmediat.getNombre() != null) {
            lResultat = aImmediat.getNombre();
        } else if (aImmediat.getIdentificateur() != null) {
            lResultat = variables.get(aImmediat.getIdentificateur());
            if (lResultat == null) {
                genererErreur(aImmediat, "Variable " + aImmediat.getIdentificateur() +
                        " n'existe pas.");
            }
        } else {
            genererErreur(aImmediat, "Valeur immediate non valide.");
        }
        return lResultat;
    }

    /**
     * Calcul et retourne la valeur d'un élément qui peut être affiché à l'utilisateur
     * @param aAffichable l'élément à afficher.
     * @return la valeur de l'élément à afficher
     * @throws InterpreterException Lorsqu'une erreur d'interprétation est survenue.
     */
    private String valeurAffichable(PsiAffichable aAffichable) throws InterpreterException {
        String lResultat = "";
        if (aAffichable instanceof PsiChaine lChaine) {
            lResultat = lChaine.getChaine();
        } else if (aAffichable instanceof PsiAfficherFinLigne) {
            lResultat = "\n";
        } else if (aAffichable instanceof PsiExpression lExpression) {
            lResultat = valeurExpression(lExpression).toString();
        } else {
            genererErreur(aAffichable, "Affichable non valide.");
        }
        return lResultat;
    }

    /**
     * Lance une exception permettant au client de la classe d'afficher un message d'erreur
     * @param aElement L'élément ayant produit l'erreur (utile pour avoir la ligne et la colone)
     * @param aMessage Le message spécifique à afficher (sans entête)
     * @throws InterpreterException L'erreur d'interprétation.
     */
    private void genererErreur(PsiElement aElement, String aMessage) throws InterpreterException {
        throw new InterpreterException("Erreur d'exécution (ligne: " + aElement.getLigne() +
                ", colonne: " + aElement.getColonne() + "): " + aMessage);
    }

}
