package interpreteur;

/**
 * Exception lancée lorsque l'interpréteur détecte une erreur.
 * @author Louis Marchand
 * @version 1.0, 8 avril 2024
 * @see AdditionneurInterpreteur
 */
public class InterpreterException extends Exception {
    /**
     * Initialisation de l'exception.
     * @param aMessage Le message à utiliser afin d'informer l'utilisateur.
     */
    public InterpreterException(String aMessage) {
        super(aMessage);
    }
}
