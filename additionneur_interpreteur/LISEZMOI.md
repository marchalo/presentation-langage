# Compilation et exécution

## Sur Linux

```bash
java -jar jflex-1.9.1.jar -d generation Additionneur.flex
java -jar java-cup-11b.jar -destdir generation -package generation -symbols Symboles -parser AdditionneurParser Additionneur.bnf
javac -cp ".:java-cup-11b-runtime.jar" Programme.java
java -cp ".:java-cup-11b-runtime.jar" Programme fichier.add
```

## Sur Windows

```bash
java -jar jflex-1.9.1.jar -d generation Additionneur.flex
java -jar java-cup-11b.jar -destdir generation -package generation -symbols Symboles -parser AdditionneurParser Additionneur.bnf
javac -cp ".:java-cup-11b-runtime.jar" Programme.java
java -cp ".;java-cup-11b-runtime.jar" Programme fichier.add
```
