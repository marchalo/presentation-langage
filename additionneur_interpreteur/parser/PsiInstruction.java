package parser;

/**
 * Noeud de l'arbre syntaxique représentant une instruction à exécuter.
 * @author Louis Marchand
 * @version 1.0, 8 avril 2024
 */
public abstract class PsiInstruction extends PsiElement {

    /**
     * Initialisation de l'élément.
     * @param aLigne Le numéro de `ligne` du jeton de l'élément (débute à 0)
     * @param aColonne Le numéro de `colonne` du jeton de l'élément (débute à 0)
     */
    protected PsiInstruction(int aLigne, int aColonne) {
        super(aLigne, aColonne);
    }
}
