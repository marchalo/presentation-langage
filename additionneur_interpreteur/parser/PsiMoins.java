package parser;

/**
 * Noeud de l'arbre syntaxique représentant l'opérateur binaire de la soustraction.
 * @author Louis Marchand
 * @version 1.0, 8 avril 2024
 */
public class PsiMoins extends PsiOperateurBinaire {

    /**
     * Initialisation de l'opérateur binaire de soustraction.
     * @param aLigne Le numéro de `ligne` du jeton de l'élément (débute à 0)
     * @param aColonne Le numéro de `colonne` du jeton de l'élément (débute à 0)
     * @param aImmediat L'élément immediat (opérande de gauche)
     * @param aExpression L'élément expression (opérande de droite)
     * @throws ParserException L'exception qui est lancé si l'initialisation est impossible.
     */
    public PsiMoins(int aLigne, int aColonne, Object aImmediat, Object aExpression)
            throws ParserException {
        super(aLigne, aColonne, aImmediat, aExpression);
    }
}
