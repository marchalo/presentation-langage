package parser;

/**
 * Classe parent de tous les noeuds et et feuille de l'arbre syntaxique.
 * @author Louis Marchand
 * @version 1.0, 8 avril 2024
 */
public abstract class PsiElement {

    /**
     * Numéro de ligne contenant le jeton (token) de l'élément.
     *
     * Débute à 1.
     */
    private int ligne;

    /**
     * Numéro de colonne où débute le jeton (token) de l'élément dans la `ligne`
     *
     * Débute à 1.
     */
    private int colonne;

    /**
     * Numéro de ligne contenant le jeton (token) de l'élément.
     *
     * Débute à 1.
     *
     * @return Le numéro de ligne
     */
    public int getLigne() {
        return ligne;
    }

    /**
     * Numéro de colonne où débute le jeton (token) de l'élément dans la `ligne`
     *
     * Débute à 1.
     * @return Le numéro de colonne.
     */
    public int getColonne() {
        return colonne;
    }

    /**
     * Utiliser pour afficher un message standard lorsque l'analyseur syntaxique échoue.
     * @param aMessage Le message spécifique à afficher (sans entête).
     * @param aObject L'objet fautif qui a été envoyé par l'analyseur.
     * @throws ParserException L'exception qui est lancé afin d'en informer l'utilisateur.
     */
    protected void gererErreurType(String aMessage, Object aObject) throws ParserException {
        String lMessage = "Erreur de syntaxe (ligne: " + ligne + " colonne: " +
                colonne + "): " + aMessage + " ";
        if (aObject != null) {
            throw new ParserException(lMessage + aObject.getClass() + " trouvé.");
        } else {
            throw new ParserException(lMessage + " null trouvé.");
        }
    }

    /**
     * Initialisation de l'élément.
     * @param aLigne Le numéro de `ligne` du jeton de l'élément (débute à 0)
     * @param aColonne Le numéro de `colonne` du jeton de l'élément (débute à 0)
     */
    protected PsiElement(int aLigne, int aColonne) {
        ligne = aLigne + 1;
        colonne = aColonne + 1;
    }

}
