package parser;

/**
 * Noeud de l'arbre syntaxique représentant l'affichage d'une valeur.
 * @author Louis Marchand
 * @version 1.0, 8 avril 2024
 */
public class PsiAffiche extends PsiInstruction{

    /**
     * L'élément qui doit être affiché.
     */
    private PsiAffichable affichable;

    /**
     * L'élément qui doit être affiché.
     * @return L'élément à afficher.
     */
    public PsiAffichable getAffichable() {
        return affichable;
    }

    /**
     * Initialisation de l'élément
     * @param aLigne Le numéro de `ligne` du jeton de l'élément (débute à 0)
     * @param aColonne Le numéro de `colonne` du jeton de l'élément (débute à 0)
     * @param aAffichable L'élément qui doit être affiché.
     * @throws ParserException L'exception qui est lancé si l'initialisation est impossible.
     */
    public PsiAffiche(int aLigne, int aColonne, Object aAffichable) throws ParserException {
        super(aLigne, aColonne);
        if (aAffichable instanceof PsiAffichable lAffichable) {
            affichable = lAffichable;
        } else {
            gererErreurType("Il est seulement possible d'afficher un affichable.", aAffichable);
        }
    }
}
