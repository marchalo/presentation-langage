package parser;

/**
 * Noeud de l'arbre syntaxique représentant l'opérateur binaire de l'addition.
 * @author Louis Marchand
 * @version 1.0, 8 avril 2024
 */
public class PsiPlus extends PsiOperateurBinaire {

    /**
     * Initialisation de l'opérateur binaire d'addition.
     * @param aLigne Le numéro de `ligne` du jeton de l'élément (débute à 0)
     * @param aColonne Le numéro de `colonne` du jeton de l'élément (débute à 0)
     * @param aImmediat L'élément immediat (opérande de gauche)
     * @param aExpression L'élément expression (opérande de droite)
     * @throws ParserException L'exception qui est lancé si l'initialisation est impossible.
     */
    public PsiPlus(int aLigne, int aColonne, Object aImmediat, Object aExpression)
            throws ParserException {
        super(aLigne, aColonne, aImmediat, aExpression);
    }
}
