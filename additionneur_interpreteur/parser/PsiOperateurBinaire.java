package parser;

/**
 * Noeud de l'arbre syntaxique représentant un opérateur binaire.
 * @author Louis Marchand
 * @version 1.0, 8 avril 2024
 */
public abstract class PsiOperateurBinaire extends PsiExpression {

    /**
     * L'élément immédiat qui correspond à l'opérande de gauche de l'opérateur binaire.
     * @return L'élément immédiat
     */
    public PsiImmediat getImmediat() {
        return immediat;
    }

    /**
     * L'élément expression qui correspond à l'opérande de droit de l'opérateur binaire.
     * @return L'élément expression
     */
    public PsiExpression getExpression() {
        return expression;
    }

    /**
     * Initialisation de l'opérateur binaire.
     * @param aLigne Le numéro de `ligne` du jeton de l'élément (débute à 0)
     * @param aColonne Le numéro de `colonne` du jeton de l'élément (débute à 0)
     * @param aImmediat L'élément immediat (opérande de gauche)
     * @param aExpression L'élément expression (opérande de droite)
     * @throws ParserException L'exception qui est lancé si l'initialisation est impossible.
     */
    protected PsiOperateurBinaire(int aLigne, int aColonne, Object aImmediat, Object aExpression)
            throws ParserException {
        super(aLigne, aColonne);
        if (aImmediat instanceof PsiImmediat lImmediat) {
            immediat = lImmediat;
        } else {
            gererErreurType(
                    "L'opérande de gauche d'un opérateur binaire doit être un immediat.",
                    aExpression);
        }
        if (aExpression instanceof PsiExpression lExpression) {
            expression = lExpression;
        } else {
            gererErreurType(
                    "L'opérande de droite d'un opérateur binaire doit être une expression.",
                    aExpression);
        }
    }

    /**
     * L'élément immédiat qui correspond à l'opérande de gauche de l'opérateur binaire.
     */
    PsiImmediat immediat;

    /**
     * L'élément expression qui correspond à l'opérande de droit de l'opérateur binaire.
     */
    PsiExpression expression;
}
