package parser;

/**
 * Feuille de l'arbre syntaxique représentant une valeur immédiate.
 * @author Louis Marchand
 * @version 1.0, 8 avril 2024
 */
public class PsiImmediat extends PsiExpression {

    /**
     * Initialisation de l'élément.
     * @param aLigne Le numéro de `ligne` du jeton de l'élément (débute à 0)
     * @param aColonne Le numéro de `colonne` du jeton de l'élément (débute à 0)
     * @param aValeur La valeur de l'élément immédiat
     * @throws ParserException L'exception qui est lancé si l'initialisation est impossible.
     */
    public PsiImmediat(int aLigne, int aColonne, Object aValeur) throws ParserException {
        super(aLigne, aColonne);
        identificateur = null;
        nombre = null;
        if (aValeur instanceof String lIdentificateur) {
            identificateur = lIdentificateur;
        } else if (aValeur instanceof Integer lNombre) {
            nombre = lNombre;
        } else {
            gererErreurType("Immediat doit être un identificateur ou un nombre.", aValeur);
        }
    }

    /**
     * L'identificateur de la variable contenant la valeur; ou null si pas une variable.
     */
    private String identificateur;

    /**
     * L'identificateur de la variable contenant la valeur; ou null si pas une variable.
     * @return L'identificateur; ou null.
     */
    public String getIdentificateur() {
        return identificateur;
    }

    /**
     * Le nombre contenant la valeur; ou null si pas un nombre.
     * @return Le nombre; ou null.
     */
    public Integer getNombre() {
        return nombre;
    }

    /**
     * Le nombre contenant la valeur; ou null si pas un nombre.
     */
    private Integer nombre;

}
