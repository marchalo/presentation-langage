package parser;

/**
 * Noeud de l'arbre syntaxique représentant une ligne du fichier.
 * @author Louis Marchand
 * @version 1.0, 8 avril 2024
 */
public class PsiLigne extends PsiElement{

    /**
     * L'élément instruction qui correspond à l'opération à exécuter dans la ligne.
     */
    private PsiInstruction instruction;

    /**
     * L'élément instruction qui correspond à l'opération à exécuter dans la ligne.
     * @return L'élément instruction
     */
    public PsiInstruction getInstruction() {
        return instruction;
    }

    /**
     * L'élément correspondant à la prochaine ligne à exécuter
     *
     * Les lignes sont organiser en structures liées. La dernière ligne
     * contient une prochaineLigne null.
     */
    private PsiLigne prochaineLigne;

    /**
     * L'élément correspondant à la prochaine ligne à exécuter
     *
     * Les lignes sont organiser en structures liées. La dernière ligne
     * contient une prochaineLigne null.
     * @return la prochaine ligne à exécuter
     */
    public PsiLigne getProchaineLigne() {
        return prochaineLigne;
    }

    /**
     * Initialisation de l'élément.
     * @param aInstruction L'instruction à exécuté dans la ligne
     * @param aProchaineLigne La prochaine ligne à exécuter (ou null si aucune)
     * @throws ParserException L'exception qui est lancé si l'initialisation est impossible.
     */
    public PsiLigne(Object aInstruction, Object aProchaineLigne) throws ParserException {
        super(((PsiElement)aInstruction).getLigne(), ((PsiElement)aInstruction).getColonne());
        if (aInstruction instanceof PsiInstruction lInstruction) {
            instruction = lInstruction;
        } else {
            gererErreurType("Une ligne doit contenir une instruction.", aInstruction);
        }
        if (aProchaineLigne instanceof PsiLigne lLigne) {
            prochaineLigne = lLigne;
        } else if (aProchaineLigne == null) {
            prochaineLigne = null;
        } else {
            gererErreurType("Ce qui suit une ligne doit être une autre ligne.", aProchaineLigne);
        }
    }
}
