package parser;

/**
 * Noeud de l'arbre syntaxique représentant un élément pouvant être afficher.
 * @author Louis Marchand
 * @version 1.0, 8 avril 2024
 */
public abstract class PsiAffichable extends PsiElement {

    /**
     * Initialisation de l'élément.
     * @param aLigne Le numéro de `ligne` du jeton de l'élément (débute à 0)
     * @param aColonne Le numéro de `colonne` du jeton de l'élément (débute à 0)
     */
    protected PsiAffichable(int aLigne, int aColonne) {
        super(aLigne, aColonne);
    }
}
