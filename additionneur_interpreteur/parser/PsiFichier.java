package parser;

/**
 * Noeud racine de l'arbre syntaxique.
 * @author Louis Marchand
 * @version 1.0, 8 avril 2024
 */
public class PsiFichier extends PsiElement{

    /**
     * La première ligne du fichier.
     */
    private PsiLigne premiereLigne;

    /**
     * La première ligne du fichier.
     * @return La ligne
     */
    public PsiLigne getPremiereLigne() {
        return premiereLigne;
    }

    /**
     * Initialisation de l'élément.
     * @param aPremiereLigne La première ligne à exécuter.
     * @throws ParserException L'exception qui est lancé si l'initialisation est impossible.
     */
    public PsiFichier(Object aPremiereLigne) throws ParserException {
        super(0, 0);
        if (aPremiereLigne instanceof PsiLigne lLigne) {
            premiereLigne = lLigne;
        } else if (aPremiereLigne == null) {
            premiereLigne = null;
        } else {
            gererErreurType("Un fichier doit contenir des lignes.", aPremiereLigne);
        }
    }
}
