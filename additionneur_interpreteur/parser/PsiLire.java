package parser;

/**
 * Feuille de l'arbre syntaxique représentant la lecture d'une valeur au clavier.
 * @author Louis Marchand
 * @version 1.0, 8 avril 2024
 */
public class PsiLire extends PsiExpression {

    /**
     * Initialisation de l'élément.
     * @param aLigne Le numéro de `ligne` du jeton de l'élément (débute à 0)
     * @param aColonne Le numéro de `colonne` du jeton de l'élément (débute à 0)
     */
    public PsiLire(int aLigne, int aColonne) {
        super(aLigne, aColonne);
    }
}
