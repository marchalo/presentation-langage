package parser;

/**
 * Exception lancée lorsque l'analyseur syntaxique ne peut terminer la création de l'arbre.
 * @author Louis Marchand
 * @version 1.0, 8 avril 2024
 */
public class ParserException extends RuntimeException {

    /**
     * Initialisation de l'exception.
     * @param aMessage Le message à utiliser afin d'informer l'utilisateur.
     */
    public ParserException(String aMessage) {
        super(aMessage);
    }
}
