package parser;

/**
 * Noeud de l'arbre syntaxique représentant l'assignation d'une valeur à une variable.
 * @author Louis Marchand
 * @version 1.0, 8 avril 2024
 */
public class PsiAssignation extends PsiInstruction {

    /**
     * L'identificateur de la variable cible de l'assignation.
     */
    private String identificateur;

    /**
     * L'identificateur de la variable cible de l'assignation.
     * @return L'identificateur de la variable cible.
     */
    public String getIdentificateur() {
        return identificateur;
    }

    /**
     * L'expression contenant la valeur à assigner.
     */
    private PsiExpression expression;

    /**
     * L'expression contenant la valeur à assigner.
     * @return L'expression de la valeur
     */
    public PsiExpression getExpression() {
        return expression;
    }

    /**
     * Initialisation de l'élément.
     * @param aLigne Le numéro de `ligne` du jeton de l'élément (débute à 0)
     * @param aColonne Le numéro de `colonne` du jeton de l'élément (débute à 0)
     * @param aIdentificateur L'identificateur de la variable cible de l'assignation.
     * @param aExpression L'expression contenant la valeur à assigner.
     * @throws ParserException L'exception qui est lancé si l'initialisation est impossible.
     */
    public PsiAssignation(int aLigne, int aColonne, Object aIdentificateur, Object aExpression)
            throws ParserException {
        super(aLigne, aColonne);
        if (aIdentificateur instanceof String lIdentificateur) {
            identificateur = lIdentificateur;
        } else {
            gererErreurType(
                    "L'opérande de gauche d'une assignation doit être un identificateur.",
                    aIdentificateur);
        }
        if (aExpression instanceof PsiExpression lExpression) {
            expression = lExpression;
        } else {
            gererErreurType(
                    "L'opérande de gauche d'une assignation doit être un identificateur.",
                    aExpression);
        }
    }
}
