package parser;

/**
 * Noeud de l'arbre syntaxique représentant l'affichage d'une fin de ligne.
 * @author Louis Marchand
 * @version 1.0, 8 avril 2024
 */
public class PsiAfficherFinLigne extends PsiAffichable{

    /**
     * Initialisation de l'élément.
     * @param aLigne Le numéro de `ligne` du jeton de l'élément (débute à 0)
     * @param aColonne Le numéro de `colonne` du jeton de l'élément (débute à 0)
     */
    public PsiAfficherFinLigne(int aLigne, int aColonne) {
        super(aLigne, aColonne);
    }
}
