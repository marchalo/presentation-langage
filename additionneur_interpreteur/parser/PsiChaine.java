package parser;

/**
 * Feuille de l'arbre syntaxique représentant un texte à afficher.
 * @author Louis Marchand
 * @version 1.0, 8 avril 2024
 */
public class PsiChaine extends PsiAffichable{

    /**
     * La valeur texte à afficher.
     */
    private String chaine;

    /**
     * La valeur texte à afficher.
     * @return Le texte.
     */
    public String getChaine() {
        return chaine;
    }

    /**
     * Initialisation de l'élément.
     * @param aLigne Le numéro de `ligne` du jeton de l'élément (débute à 0)
     * @param aColonne Le numéro de `colonne` du jeton de l'élément (débute à 0)
     * @param aChaine La valeur texte à afficher.
     * @throws ParserException L'exception qui est lancé si l'initialisation est impossible.
     */
    public PsiChaine(int aLigne, int aColonne, Object aChaine) throws ParserException {
        super(aLigne, aColonne);
        if (aChaine instanceof String lChaine) {
            chaine = lChaine.substring(1, lChaine.length() - 1);
        } else {
            gererErreurType("Une chaîne doit être de type String.", aChaine);
        }
    }
}
