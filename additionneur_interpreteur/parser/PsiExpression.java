package parser;

/**
 * Noeud de l'arbre syntaxique représentant un expression.
 *
 * Une expression permet de calculer une valeur numérique.
 *
 * @author Louis Marchand
 * @version 1.0, 8 avril 2024
 */
public abstract class PsiExpression extends PsiAffichable {

    /**
     * Initialisation de l'élément.
     * @param aLigne Le numéro de `ligne` du jeton de l'élément (débute à 0)
     * @param aColonne Le numéro de `colonne` du jeton de l'élément (débute à 0)
     */
    protected PsiExpression(int aLigne, int aColonne) {
        super(aLigne, aColonne);
    }
}
