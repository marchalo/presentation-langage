package generation;

import java_cup.runtime.*;

%%

%class AdditionneurLexer

%line
%column

%cup

%eofclose

%{
    private Symbol symbole(int type) {
        return new Symbol(type, yyline, yycolumn);
    }

    private Symbol symbole(int type, Object value) {
        return new Symbol(type, yyline, yycolumn, value);
    }
%}

%eofval{
new java_cup.runtime.Symbol(Symboles.EOF);
%eofval}

%%

	"+"                       { return symbole(Symboles.PLUS); }

	"-"                       { return symbole(Symboles.MOINS); }

	"<-"                      { return symbole(Symboles.ASSIGNATEUR); }

	[\r\n]                    { return symbole(Symboles.FIN_DE_LIGNE); }

	"affiche"                 { return symbole(Symboles.AFFICHE); }

	"fin_de_ligne"            { return symbole(Symboles.AFFICHE_FIN_LIGNE); }

	"lire_entier"             { return symbole(Symboles.LIRE); }

	[1-9][0-9]*               { return symbole(Symboles.NOMBRE, Integer.parseInt(yytext())); }

	\"[^\"\n]*\"              { return symbole(Symboles.CHAINE, new String(yytext())); }

	[a-zA-Z][a-zA-Z0-9_]*     { return symbole(Symboles.IDENTIFICATEUR, new String(yytext())); }

	[ \t\f]                   { /* ignorer */ }   

	#[^\r\n]*                 { /* ignorer */ }

	<<EOF>>                   { return symbole(Symboles.EOF); }


[^]                           { throw new Error("Illegal character <"+yytext()+">"); }
