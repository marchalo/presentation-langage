package com.tioui.additionneur.sdk.lexer;

import com.intellij.lexer.FlexAdapter;

/**
 * Un adaptateur utilisé pour lier l'analyseur syntaxique (Parser)
 * avec l'analyseur lexical (Lexer).
 * @author Louis Marchand
 * @version 1.0, 10 avril 2024
 */
public class AdditionneurLexerAdapteur extends FlexAdapter {

    /**
     * Initialisation de l'objet.
     */
    public AdditionneurLexerAdapteur() {
        super(new AdditionneurLexer(null));
    }

}
