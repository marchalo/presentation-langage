package com.tioui.additionneur.sdk;

import com.intellij.openapi.fileTypes.SyntaxHighlighter;
import com.intellij.openapi.fileTypes.SyntaxHighlighterFactory;
import com.intellij.openapi.project.Project;
import com.intellij.openapi.vfs.VirtualFile;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

/**
 * Permet la création d'un surligneur de syntaxe (syntax highlighting) pour le langage Additionneur.
 * @author Louis Marchand
 * @version 1.0, 12 avril 2024
 */
public class AdditionneurSyntaxHighlighterFactory extends SyntaxHighlighterFactory {
    /**
     * Retourn un nouveau surligneur de syntaxe pour le langage Additionneur.
     * @param aProjet Le projet IntelliJ en cours (non utilisé)
     * @param aFichier Le fichier ouvert dans le projet. (non utilisé)
     * @return Le surligneur de syntaxe
     */
    @Override
    public @NotNull SyntaxHighlighter getSyntaxHighlighter(@Nullable Project aProjet,
                                                           @Nullable VirtualFile aFichier) {
        return new AdditionneurSyntaxHighlighter();
    }
}
