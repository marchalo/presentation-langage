package com.tioui.additionneur.sdk;

import com.intellij.lexer.Lexer;
import com.intellij.openapi.editor.DefaultLanguageHighlighterColors;
import com.intellij.openapi.editor.HighlighterColors;
import com.intellij.openapi.editor.colors.TextAttributesKey;
import static com.intellij.openapi.editor.colors.TextAttributesKey.createTextAttributesKey;
import com.intellij.openapi.fileTypes.SyntaxHighlighterBase;
import com.intellij.psi.TokenType;
import com.intellij.psi.tree.IElementType;
import com.tioui.additionneur.sdk.lexer.AdditionneurLexerAdapteur;
import com.tioui.additionneur.sdk.psi.AdditionneurTypes;
import org.jetbrains.annotations.NotNull;

/**
 * Permet de surligner la syntaxe (syntax highlighting) du langage Additionneur.
 * @author Louis Marchand
 * @version 1.0, 12 avril 2024
 */
public class AdditionneurSyntaxHighlighter  extends SyntaxHighlighterBase {

    /**
     * Constantes qui représentent chaque attributs de texte individuellement.
     */
    public static final TextAttributesKey OPERATEURS =
            createTextAttributesKey("OPERATEURS", DefaultLanguageHighlighterColors.OPERATION_SIGN);
    public static final TextAttributesKey MOTS_CLE =
            createTextAttributesKey("MOTS_CLE", DefaultLanguageHighlighterColors.KEYWORD);
    public static final TextAttributesKey CHAINE =
            createTextAttributesKey("CHAINE", DefaultLanguageHighlighterColors.STRING);
    public static final TextAttributesKey NOMBRE =
            createTextAttributesKey("NOMBRE", DefaultLanguageHighlighterColors.NUMBER);
    public static final TextAttributesKey IDENTIFICATEUR =
            createTextAttributesKey("IDENTIFICATEUR", DefaultLanguageHighlighterColors.IDENTIFIER);
    public static final TextAttributesKey MAUVAIS_CARACTERE =
            createTextAttributesKey("MAUVAIS_CARACTERE", HighlighterColors.BAD_CHARACTER);

    /**
     * Constantes qui représentent chaque attributs de texte sous la forme de tableau.
     */
    private static final TextAttributesKey[] MAUVAIS_CARACTERE_KEYS =
            new TextAttributesKey[]{MAUVAIS_CARACTERE};
    private static final TextAttributesKey[] OPERATEURS_KEYS = new TextAttributesKey[]{OPERATEURS};
    private static final TextAttributesKey[] IDENTIFICATEUR_KEYS = new TextAttributesKey[]{IDENTIFICATEUR};
    private static final TextAttributesKey[] MOTS_CLE_KEYS = new TextAttributesKey[]{MOTS_CLE};
    private static final TextAttributesKey[] CHAINE_KEYS = new TextAttributesKey[]{CHAINE};
    private static final TextAttributesKey[] NOMBRE_KEYS = new TextAttributesKey[]{NOMBRE};
    private static final TextAttributesKey[] AUCUN_KEYS = new TextAttributesKey[0];

    /**
     * L'analyseur lexical utilisé pour obtenir les jetons.
     * @return l'analyseur lexical.
     */
    @Override
    public @NotNull Lexer getHighlightingLexer() {
        return new AdditionneurLexerAdapteur();
    }

    /**
     * Indique au surligneur quel attribut de texte doit être associé à un jeton en particlulier.
     * @param aJeton Le jeton à associer
     * @return l'attribut de texte associé au jeton.
     */
    @Override
    public TextAttributesKey @NotNull [] getTokenHighlights(IElementType aJeton) {
        TextAttributesKey @NotNull [] lResultat;
        if (aJeton.equals(AdditionneurTypes.PLUS)) {
            lResultat = OPERATEURS_KEYS;
        } else if (aJeton.equals(AdditionneurTypes.MOINS)) {
            lResultat = OPERATEURS_KEYS;
        } else if (aJeton.equals(AdditionneurTypes.ASSIGNATEUR)) {
            lResultat = OPERATEURS_KEYS;
        } else if (aJeton.equals(AdditionneurTypes.AFFICHE)) {
            lResultat = MOTS_CLE_KEYS;
        } else if (aJeton.equals(AdditionneurTypes.LIRE)) {
            lResultat = MOTS_CLE_KEYS;
        } else if (aJeton.equals(AdditionneurTypes.NOMBRE)) {
            lResultat = NOMBRE_KEYS;
        } else if (aJeton.equals(AdditionneurTypes.CHAINE)) {
            lResultat = CHAINE_KEYS;
        } else if (aJeton.equals(AdditionneurTypes.AFFICHE_FIN_LIGNE)) {
            lResultat = CHAINE_KEYS;
        } else if (aJeton.equals(AdditionneurTypes.IDENTIFICATEUR)) {
            lResultat = IDENTIFICATEUR_KEYS;
        } else if (aJeton.equals(TokenType.BAD_CHARACTER)) {
            lResultat = MAUVAIS_CARACTERE_KEYS;
        } else {
            lResultat = AUCUN_KEYS;
        }
        return lResultat;
    }
}
