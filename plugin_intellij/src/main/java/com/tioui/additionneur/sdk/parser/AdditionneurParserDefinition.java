package com.tioui.additionneur.sdk.parser;

import com.intellij.lang.ASTNode;
import com.intellij.lang.ParserDefinition;
import com.intellij.lang.PsiParser;
import com.intellij.lexer.Lexer;
import com.intellij.openapi.project.Project;
import com.intellij.psi.FileViewProvider;
import com.intellij.psi.PsiElement;
import com.intellij.psi.PsiFile;
import com.intellij.psi.tree.IFileElementType;
import com.intellij.psi.tree.TokenSet;
import com.tioui.additionneur.sdk.AdditionneurFile;
import com.tioui.additionneur.sdk.AdditionneurLanguage;
import com.tioui.additionneur.sdk.lexer.AdditionneurLexerAdapteur;
import com.tioui.additionneur.sdk.psi.AdditionneurTypes;
import org.jetbrains.annotations.NotNull;

/**
 * Informations (définition) à propos de l'analyseur syntaxique.
 * @author Louis Marchand
 * @version 1.0, 10 avril 2024
 */
public class AdditionneurParserDefinition implements ParserDefinition {

  public static final IFileElementType FILE = new IFileElementType(AdditionneurLanguage.INSTANCE);

  /**
   * Création de l'analyseur lexical qui fournira l'analyseur syntaxique en jeton.
   * @param project Le projet IntelliJ
   * @return L'analyseur lexical
   */
  @NotNull
  @Override
  public Lexer createLexer(Project project) {
    return new AdditionneurLexerAdapteur();

  }

  /**
   * Jeton utilisé comme commentaire (non utilisé présentement)
   * @return Le jeton commentaire
   */
  @NotNull
  @Override
  public TokenSet getCommentTokens() {
    return TokenSet.EMPTY;
  }

  /**
   * Le jeton utilisé comme chaîne de caractères (non utilisé présentement)
   * @return L'élément utilisé comme chaîne de caractères
   */
  @NotNull
  @Override
  public TokenSet getStringLiteralElements() {
    return TokenSet.EMPTY;
  }

  /**
   * Création de l'analyseur syntaxique
   * @param project Le projet IntelliJ
   * @return L'analiseur syntaxique
   */
  @NotNull
  @Override
  public PsiParser createParser(final Project project) {
    return new AdditionneurParser();
  }

  /**
   * Noeud racine de l'arbre syntaxique (Fichier)
   * @return Le noeud racine
   */
  @NotNull
  @Override
  public IFileElementType getFileNodeType() {
    return FILE;
  }

  /**
   * Création d'un fichier de type additionneur
   * @param viewProvider L'objet permettant d'obtenir un nouveau fichier
   * @return Le fichier additionneur
   */
  @NotNull
  @Override
  public PsiFile createFile(@NotNull FileViewProvider viewProvider) {
    return new AdditionneurFile(viewProvider);
  }

  /**
   * Création d'un élément dans l'Arbre syntaxique.
   * @param node Le type de noeud à créer dans l'arbre
   * @return L'élément de l'arbre syntaxique.
   */
  @NotNull
  @Override
  public PsiElement createElement(ASTNode node) {
    return AdditionneurTypes.Factory.createElement(node);
  }

}
