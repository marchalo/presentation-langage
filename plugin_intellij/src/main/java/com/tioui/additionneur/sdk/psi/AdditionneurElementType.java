package com.tioui.additionneur.sdk.psi;

import com.intellij.psi.tree.IElementType;
import com.tioui.additionneur.sdk.AdditionneurLanguage;
import org.jetbrains.annotations.NonNls;
import org.jetbrains.annotations.NotNull;

/**
 * Type d'élément dans l'arbre syntaxique.
 * @author Louis Marchand
 * @version 1.0, 10 avril 2024
 */
public class AdditionneurElementType extends IElementType {

    /**
     * Initialisation de l'objet
     * @param debugName Chaîne utilisée pour afficher un message de débuggage (non utilisée)
     */
    public AdditionneurElementType(@NotNull @NonNls String debugName) {
        super(debugName, AdditionneurLanguage.INSTANCE);
    }

}
