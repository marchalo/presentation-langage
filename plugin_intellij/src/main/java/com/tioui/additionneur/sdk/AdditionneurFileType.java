package com.tioui.additionneur.sdk;

import com.intellij.openapi.fileTypes.LanguageFileType;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import javax.swing.Icon;

/**
 * Un type fichier source du langage additionneur.
 * @author Louis Marchand
 * @version 1.0, 10 avril 2024
 */
public class AdditionneurFileType extends LanguageFileType {

    /**
     * Instance unique du type
     */
    public static final AdditionneurFileType INSTANCE = new AdditionneurFileType();

    /**
     * Initialisation de l'objet.
     */
    private AdditionneurFileType() {
        super(AdditionneurLanguage.INSTANCE);
    }

    /**
     * Nom du type de fichier additionneur.
     * @return le nom du type de fichier.
     */
    @NotNull
    @Override
    public String getName() {
        return "Additionneur source file";
    }

    /**
     * La description du type de fichier source du langage additionneur
     * @return la description du type de fichier
     */
    @NotNull
    @Override
    public String getDescription() {
        return "Additionneur language file";
    }

    /**
     * L'extension du type de fichier source du langage additionneur.
     * @return l'extension du type de fichier.
     */
    @NotNull
    @Override
    public String getDefaultExtension() {
        return "add";
    }

    /**
     * L'icone à utiliser pour le type de fichier source du langage additionneur.
     * @return l'icone représentant le type de fichier.
     */
    @Nullable
    @Override
    public Icon getIcon() {
        return AdditionneurIcons.FILE;
    }
}
