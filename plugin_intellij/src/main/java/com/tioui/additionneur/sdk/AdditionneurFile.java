

package com.tioui.additionneur.sdk;

import com.intellij.extapi.psi.PsiFileBase;
import com.intellij.openapi.fileTypes.FileType;
import com.intellij.psi.FileViewProvider;
import org.jetbrains.annotations.NotNull;

import java.util.ArrayList;

/**
 * Un fichier source du langage additionneur.
 * @author Louis Marchand
 * @version 1.0, 10 avril 2024
 */
public class AdditionneurFile  extends PsiFileBase {

    /**
     * Initialisation du fichier additionneur.
     * @param viewProvider Permet la création d'un fichier IntelliJ.
     */
    public AdditionneurFile(@NotNull FileViewProvider viewProvider) {
        super(viewProvider, AdditionneurLanguage.INSTANCE);
    }

    /**
     * Retourn el type de fichier.
     * @return le type du fichier.
     */
    @NotNull
    @Override
    public FileType getFileType() {
        return AdditionneurFileType.INSTANCE;
    }

    /**
     * Représentation texte de l'objet en cours.
     * @return Chaîne représentant l'objet en cours.
     */
    @Override
    public String toString() {
        return "Additionneur source file";
    }

}
