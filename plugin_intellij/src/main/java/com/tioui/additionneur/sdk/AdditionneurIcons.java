package com.tioui.additionneur.sdk;

import com.intellij.openapi.util.IconLoader;

import javax.swing.Icon;

/**
 * Icône des fichiers sources du langage additionneur.
 * @author Louis Marchand
 * @version 1.0, 10 avril 2024
 */
public class AdditionneurIcons {

    /**
     * Singleton du type en cours
     */
    public static final Icon FILE = IconLoader.getIcon("/images/add_icon.png",
            AdditionneurIcons.class);

}
