package com.tioui.additionneur.sdk.psi;

import com.intellij.psi.tree.IElementType;
import com.tioui.additionneur.sdk.AdditionneurLanguage;
import org.jetbrains.annotations.NonNls;
import org.jetbrains.annotations.NotNull;

/**
 * Type de jeton utilisé dans l'analyseur lexical. Nécessaire mais non utilisé.
 * @author Louis Marchand
 * @version 1.0, 10 avril 2024
 */
public class AdditionneurTokenType extends IElementType {

    /**
     * Initialisation de l'objet
     * @param debugName Chaîne utilisée pour afficher un message de débuggage (non utilisée)
     */
    public AdditionneurTokenType(@NotNull @NonNls String debugName) {
        super(debugName, AdditionneurLanguage.INSTANCE);
    }

    /**
     * Permet l'affichage de l'objet en cours.
     * @return la chaîne représentant l'objet en cours.
     */
    @Override
    public String toString() {
        return "AdditionneurTokenType." + super.toString();
    }

}

