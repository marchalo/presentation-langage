package com.tioui.additionneur.sdk;


import com.intellij.lang.Language;

/**
 * Classe représentant le langage additionneur.
 * @author Louis Marchand
 * @version 1.0, 10 avril 2024
 */
public class AdditionneurLanguage extends Language {

    /**
     * Singleton du type en cours.
     */
    public static final AdditionneurLanguage INSTANCE = new AdditionneurLanguage();

    /**
     * Initialisation de l'objet.
     */
    private AdditionneurLanguage() {
        super("Additionneur");
    }

}
