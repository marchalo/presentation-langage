package com.tioui.additionneur.sdk.lexer;

import com.intellij.lexer.FlexLexer;
import com.intellij.psi.tree.IElementType;
import com.tioui.additionneur.sdk.psi.AdditionneurTypes;
import com.intellij.psi.TokenType;

%%

%class AdditionneurLexer
%implements FlexLexer
%unicode
%function advance
%type IElementType
%eof{  return;
%eof}

%%


"+"                              { return AdditionneurTypes.PLUS; }

"-"                              { return AdditionneurTypes.MOINS; }

"<-"                             { return AdditionneurTypes.ASSIGNATEUR; }

[\r\n]                           { return AdditionneurTypes.FIN_DE_LIGNE; }

"affiche"                        { return AdditionneurTypes.AFFICHE; }

"fin_de_ligne"                   { return AdditionneurTypes.AFFICHE_FIN_LIGNE; }

"lire_entier"                    { return AdditionneurTypes.LIRE; }

[1-9][0-9]*                      { return AdditionneurTypes.NOMBRE; }

\"[^\"\n]*\"                     { return AdditionneurTypes.CHAINE; }

[a-zA-Z][a-zA-Z0-9_]*            { return AdditionneurTypes.IDENTIFICATEUR; }

#[^\r\n]*                        { return TokenType.WHITE_SPACE; }

[\ \t]                           { return TokenType.WHITE_SPACE; }

[^]                              { return TokenType.BAD_CHARACTER; }