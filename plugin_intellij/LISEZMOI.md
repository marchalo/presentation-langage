Plugin IntelliJ pour le langage additionneur
============================================

Pour générer l'analyseur lexical:

- Dans IntelliJ, appuyer sur le bouton de droite sur le fichier "Additionneur.flex" (dans le répertoire "src/main/java/com/tioui/additionneur/sdk/lexer");
- Sélectionner "Run JFlex generator".

Pour générer l'analyseur syntaxique:

- Ouvrir le projet dans IntelliJ;
- Ensuite, appuyer sur le bouton de droite sur le fichier "Additionneur.bnf" (dans le répertoire "src/main/java/com/tioui/additionneur/sdk/parser");
- Sélectionner "Generate parser code".

Pour lancer l'éditeur avec le plugin installé:

- Ouvrir un terminal positionné dans le répertoire contenant ce LISEZMOI.
- Sur Linux, utiliser la commande:

```bash
./gradlew runIde
```

-Sur Windows, utilisez la commande:

```bash
gradlew.bat runIde
```
